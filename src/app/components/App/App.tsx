import React from 'react';
import '../../../shared/styles/reset.css';
import { AppRouters, ROUTES } from '../../app.routers';
import './App.css';

const App = () => {
  return (
    <div className="App">
      <AppRouters routes={ROUTES} />
    </div>
  );
};

export default App;
