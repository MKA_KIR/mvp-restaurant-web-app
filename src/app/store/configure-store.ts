import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import rootReducer from './root-reducer';

const middleware = [...getDefaultMiddleware()];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: process.env.NODE_ENV !== 'production',
});

declare const module: any;

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./root-menuReducer', () => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires,global-require
    const newRootReducer = require('./root-reducer').default;
    store.replaceReducer(newRootReducer);
  });
}

export type AppDispatch = typeof store.dispatch;

export default store;
