import { combineReducers } from '@reduxjs/toolkit';
import products from '../../dashboard/products/reducer/products.reducer';

const rootReducer = combineReducers({
  products,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
