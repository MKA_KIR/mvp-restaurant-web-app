import { combineReducers } from 'redux';

import { menuReducer } from '../../client/Menu/reducer/menu.reducer';

export default combineReducers({
  menu: menuReducer,
});
