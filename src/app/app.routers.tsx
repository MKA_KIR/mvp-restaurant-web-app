import React, { FC, PropsWithChildren } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Button, Result } from 'antd';

import ProductsPage from '../dashboard/products/pages/ProductsPage';
import CategoryListPage from "../client/Menu/pages/CategoryListPage";
import DishListPage from "../client/Menu/pages/DishListPage";
import DishPage from "../client/Menu/pages/DishPage";

export interface IAppRoutes {
  routes: Record<string, any>[];
}

const RouteWithSubRoutes: FC<PropsWithChildren<any>> = (route) => (
  <Route
    path={route.path}
    exact={route.exact}
    render={(props) => <route.component {...props} routes={route.routes} />}
  />
);

export const AppRouters: FC<PropsWithChildren<IAppRoutes>> = ({
  routes,
}: Partial<IAppRoutes>) => (
  <Switch>
    {routes?.map((route: any) => {
      return <RouteWithSubRoutes key={route.key} {...route} />;
    })}
    <Route path="*">
      <Redirect to="/404" />
    </Route>
  </Switch>
);

export const ROUTES = [
  {
    path: '/',
    key: 'HOME',
    exact: true,
    component: CategoryListPage,
  },
  {
    exact: true,
    path: '/404',
    key: 'NOT_FOUND_PAGE',
    component: (props: any) => (
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Button type="primary" onClick={() => props.history.push('/')}>
            Back Home
          </Button>
        }
      />
    ),
  },
  {
    path: '/products',
    key: 'PRODUCTS',
    exact: true,
    component: ProductsPage,
  },
  {
    path: '/category-list-page',
    key: 'CATEGORY-LIST-PAGE',
    exact: true,
    component: CategoryListPage
  },
  {
    path: '/categories/:categoryName',
    key: 'DISH-LIST-PAGE',
    exact: true,
    component: DishListPage,
  },
  {
    path: '/products/:id',
    key: 'DISH-PAGE',
    exact: true,
    component: DishPage,
  },
  {
    key: 'ADMIN',
    component: (props: any) => {
      const {
        location: { pathname },
      } = props;
      const accessToken = Boolean(localStorage.getItem('accessToken'));
      if (!accessToken && pathname.includes('admin')) {
        return <Redirect to="/" />;
      }
      return <AppRouters {...props} />;
    },
    routes: [
      {
        path: '/admin/',
        key: 'ADMIN_DASHBOARD',
        exact: true,
        component: () => <h1>DashboardPage</h1>,
      },
    ],
  },
];
