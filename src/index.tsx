import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import * as serviceWorker from './serviceWorker';

import store from './app/store/store';

declare const module: any;

const render = () => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires,global-require
  const App = require('./app/components/App').default;

  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
  );
};

render();

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./app/components/App', render);
}

serviceWorker.unregister();
