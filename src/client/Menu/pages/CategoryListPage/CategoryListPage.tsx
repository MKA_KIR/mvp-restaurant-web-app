import React from 'react';
import SearchPanel from '../../components/SearchPanel';
import CategoryList from '../../components/CategoryList';

const CategoryListPage = () => {
  return (
    <>
      <SearchPanel />
      <CategoryList />
    </>
  );
};

export default CategoryListPage;
