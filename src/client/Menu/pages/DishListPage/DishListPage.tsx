import React from 'react';
import SearchPanel from '../../components/SearchPanel';
import DishList from '../../components/DishList';

const DishListPage = () => {
  return (
    <>
      <SearchPanel />
      <DishList />
    </>
  );
};

export default DishListPage;
