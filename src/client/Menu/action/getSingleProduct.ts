import axios from 'axios';

import {
  FETCH_REQUEST,
  FETCH_FAILURE,
  FETCH_SINGLE_PRODUCT_SUCCESS,
} from '../constants';

export const getSingleProduct = (id: any) => {
  // @ts-ignore
  const func = async (dispatch) => {
    dispatch({ type: FETCH_REQUEST });
    try {
      const { data } = await axios.get(`http://localhost:3000/products/${id}`);
      dispatch({ type: FETCH_SINGLE_PRODUCT_SUCCESS, payload: data });
    } catch (err) {
      dispatch({ type: FETCH_FAILURE, payload: err });
    }
  };
  return func;
};
