import axios from 'axios';

import {
  FETCH_REQUEST,
  FETCH_FAILURE,
  FETCH_CATEGORIES_SUCCESS,
} from '../constants';

export const getCategories = () => {
  // @ts-ignore
  const func = async (dispatch) => {
    dispatch({ type: FETCH_REQUEST });
    try {
      const { data } = await axios.get('http://localhost:3000/categories');
      dispatch({ type: FETCH_CATEGORIES_SUCCESS, payload: data });
    } catch (err) {
      dispatch({ type: FETCH_FAILURE, payload: err });
    }
  };
  return func;
};
