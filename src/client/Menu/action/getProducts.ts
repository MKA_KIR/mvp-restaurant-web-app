import axios from 'axios';

import {
  FETCH_REQUEST,
  FETCH_FAILURE,
  FETCH_PRODUCTS_SUCCESS,
} from '../constants';

export const getProducts = (categoryName: any) => {
  // @ts-ignore
  const func = async (dispatch) => {
    dispatch({ type: FETCH_REQUEST });
    try {
      const { data } = await axios.get(
        `http://localhost:3000/products?category=${categoryName}`,
      );
      dispatch({ type: FETCH_PRODUCTS_SUCCESS, payload: data });
    } catch (err) {
      dispatch({ type: FETCH_FAILURE, payload: err });
    }
  };
  return func;
};
