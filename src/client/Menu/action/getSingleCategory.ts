import axios from 'axios';

import {
  FETCH_REQUEST,
  FETCH_FAILURE,
  FETCH_SINGLE_CATEGORY_SUCCESS,
} from '../constants';

export const getSingleCategory = (categoryName: any) => {
  // @ts-ignore
  const func = async (dispatch) => {
    dispatch({ type: FETCH_REQUEST });
    try {
      const { data } = await axios.get(
        `http://localhost:3000/categories?url=${categoryName}`,
      );
      dispatch({ type: FETCH_SINGLE_CATEGORY_SUCCESS, payload: data[0] });
    } catch (err) {
      dispatch({ type: FETCH_FAILURE, payload: err });
    }
  };
  return func;
};
