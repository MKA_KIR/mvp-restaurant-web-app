import { CHANGE_PRODUCT_PRICE } from '../constants';

export const changePrice = (addCost: any) => {
  return {
    type: CHANGE_PRODUCT_PRICE,
    payload: addCost,
  };
};
