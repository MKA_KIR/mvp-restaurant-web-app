import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

import DishListHeading from '../DishListHeading';
import DishListItem from '../DishListItem';

import { getProducts, getSingleCategory } from '../../action';

const DishList = () => {
  const { categoryName } = useParams();
  // @ts-ignore
  const products = useSelector(({ menu }) => menu.products, shallowEqual);
  // @ts-ignore
  const category = useSelector(({ menu }) => menu.category, shallowEqual);
  // @ts-ignore
  const loading = useSelector(({ menu }) => menu.loading, shallowEqual);

  const dispatch = useDispatch();
  // @ts-ignore
  useEffect(() => {
    dispatch(getSingleCategory(categoryName));
    dispatch(getProducts(categoryName));
  }, []);

  // @ts-ignore
  const dishElements = products.map((item) => (
    <DishListItem key={item.id} {...item} />
  ));
  return (
    <div className="container">
      {loading && <p>loading...</p>}
      <DishListHeading {...category} />
      <div className="dish-list">{dishElements}</div>
    </div>
  );
};
export default DishList;
