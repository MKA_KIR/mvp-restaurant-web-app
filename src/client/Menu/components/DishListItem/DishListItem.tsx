import React, { FC, PropsWithChildren, useState } from 'react';

import { Link } from 'react-router-dom';
import './DishListItem.scss';
import DishActions from '../DishActions';

interface IDishListItem {
  img: any;
  active: any;
  name: any;
  description: any;
  price: any;
  id: any;
}

const DishListItem: FC<PropsWithChildren<IDishListItem>> = ({
  img,
  active,
  name,
  description,
  price,
  id,
}: Partial<IDishListItem>) => {
  const dishBgImg = {
    backgroundImage: `url(${img})`,
  };
  const [productCount, setProductCount] = useState(1);

  const increment = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    setProductCount((prevState) => prevState + 1);
  };
  const decrement = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    setProductCount((prevState) => (prevState === 1 ? 1 : prevState - 1));
  };

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return (
    <div className={`dish-list-item ${!active ? 'inactive' : ''}`}>
      <Link
        to={{ pathname: `/products/${id}`, state: { count: productCount } }}
        className="dish-list-img"
        style={dishBgImg}
      ></Link>
      <div className="dish-list-item-info">
        <div className="dish-list-item-heading">
          <Link
            to={{ pathname: `/products/${id}`, state: { count: productCount } }}
            className="dish-list-item-title"
          >
            {name}
          </Link>
          <p className="dish-list-item-price">{price} грн</p>
        </div>
        <p className="dish-list-item-description">{description}</p>
        <DishActions
          active={active}
          count={productCount}
          increment={increment}
          decrement={decrement}
          type="short"
        />
      </div>
    </div>
  );
};
export default DishListItem;
