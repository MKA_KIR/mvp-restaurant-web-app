import React, { FC, PropsWithChildren } from 'react';
import './DishActions.scss';

import { ReactComponent as LikeBtn } from '../../../../assets/images/icons/like.svg';

interface IDishActions {
  active: any;
  type: any;
  increment: any;
  decrement: any;
  count: any;
}

const DishActions: FC<PropsWithChildren<IDishActions>> = ({
  active,
  type,
  increment,
  decrement,
  count,
}: Partial<IDishActions>) => {
  if (!active) {
    return <></>;
  }
  return (
    <div className="dish-actions">
      <div className="dish-actions-col">
        <a href="#" className="dish-actions-count-btn" onClick={decrement}>
          -
        </a>
        <span className="dish-actions-count-number">{count}</span>
        <a href="#" className="dish-actions-count-btn" onClick={increment}>
          +
        </a>
      </div>
      <div className="dish-actions-col">
        <div className="dish-actions-like-btn">
          {type === 'full' ? <LikeBtn /> : ''}
        </div>
        <div>
          <a href="#" className="dish-actions-btn">
            Купити
          </a>
        </div>
      </div>
    </div>
  );
};

export default DishActions;
