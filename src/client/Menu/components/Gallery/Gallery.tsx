import React, { FC, PropsWithChildren } from 'react';

import './Gallery.scss';
import GalleryItem from '../GalleryItem';

interface IGalery {
  name: any;
  galleryProducts: any;
}

const Gallery: FC<PropsWithChildren<IGalery>> = ({
  name,
  galleryProducts,
}: Partial<IGalery>) => {
  const galleryProductsElements = galleryProducts.map((item: any) => (
    <GalleryItem key={item._id} {...item} />
  ));
  return (
    <div className="gallery-container">
      <p className="gallery-name">{name}</p>
      <div className="gallery-list">{galleryProductsElements}</div>
    </div>
  );
};

export default Gallery;
