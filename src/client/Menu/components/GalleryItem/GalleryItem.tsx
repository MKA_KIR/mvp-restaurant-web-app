import React, { FC, PropsWithChildren } from 'react';
import './GalleryItem.scss';

interface IGaleryItem {
  img: any;
  name: any;
  price: any;
}

const GalleryItem: FC<PropsWithChildren<IGaleryItem>> = ({
  img,
  name,
  price,
}: Partial<IGaleryItem>) => {
  return (
      <div className="gallery-list-item">
        <div
          className="gallery-list-img"
          style={{ backgroundImage: `url(${img})` }}
        ></div>
        <a href="#" className="gallery-list-btn">
          +
        </a>
        <p className="gallery-list-title">{name}</p>
        <p className="gallery-list-price">{price} грн</p>
      </div>
  );
};

export default GalleryItem;
