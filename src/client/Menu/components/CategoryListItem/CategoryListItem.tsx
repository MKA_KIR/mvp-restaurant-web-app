import React from 'react';
import './CategoryListItem.scss';
import { Link } from 'react-router-dom';

// @ts-ignore
// eslint-disable-next-line react/prop-types
const CategoryListItem = ({ icon, name, url, img }) => {
  return (
    <div className="category-list-item">
      <div className="category-list-heading">
        <span className="category-list-icon-container">
          <img src={icon} alt={name} />
        </span>
        <Link to={`categories/${url}`} className="category-list-title">
          <h2>{name}</h2>
        </Link>
      </div>
      <Link to={`categories/${url}`}>
        <img className="img-fluid" src={img} alt={name} />
      </Link>
    </div>
  );
};

export default CategoryListItem;
