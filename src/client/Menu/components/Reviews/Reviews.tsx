import React, { FC, PropsWithChildren, useState } from 'react';
import ReviewsItem from '../ReviewsItem';

import FormAddReview from '../FormAddReview';

import './Reviews.scss';

interface IReviews {
  reviewList: any;
}

const Reviews: FC<PropsWithChildren<IReviews>> = ({
  reviewList,
}: Partial<IReviews>) => {
  const [showTestimonial, setShowTestimonial] = useState(false);
  const reviewElements = reviewList.map((item: any) => (
    <ReviewsItem key={item._id} {...item} />
  ));
  return (
    <div className="reviews-container">
      <h3 className="reviews-title">Відгуки</h3>
      <div className="reviews-list">
        {reviewElements}
        <span className="show-all-reviews">Всі відгуки</span>
      </div>
      <span
        className="send-review"
        onClick={() => setShowTestimonial(!showTestimonial)}
      >
        Залишити відгук
      </span>
      <div>
        {showTestimonial && <FormAddReview />}
      </div>
    </div>
  );
};

export default Reviews;
