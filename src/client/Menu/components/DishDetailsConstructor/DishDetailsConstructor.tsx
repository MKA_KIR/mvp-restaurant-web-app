import React, { FC, PropsWithChildren, useState } from 'react';
import './DishDetailsConstructor.scss';
import { useDispatch } from 'react-redux';

import { changePrice } from '../../action';

export interface IDishDetailsConstructorProps {
  additionalIngridients: any;
}

const DishDetailsConstructor: FC<PropsWithChildren<
  IDishDetailsConstructorProps
>> = ({ additionalIngridients }: Partial<IDishDetailsConstructorProps>) => {
  const dispatch = useDispatch();
  const fullAdditionalIngridients = additionalIngridients.map((item: any) => ({
    ...item,
    active: false,
  }));
  const [constructorItems, setConstructorItems] = useState(
    fullAdditionalIngridients,
  );

  const handleToggle = (idx: any) => {
    const items = constructorItems.map((item: any) => ({ ...item }));
    items[idx].active = !items[idx].active;
    setConstructorItems(items);
    const addCost = items[idx].active ? items[idx].price : -items[idx].price;
    dispatch(changePrice(addCost));
  };

  const constructorItemElements = constructorItems.map(
    (item: any, index: any) => (
      <div className="dish-details-constructor-item" key={item._id}>
        <div
          className={`dish-details-constructor-checkbox-container ${
            item.active ? 'active' : ''
          }`}
        >
          <span
            className={`dish-details-checkbox ${item.active ? 'active' : ''}`}
            onClick={() => handleToggle(index)}
          />
          <span className="dish-details-label">{item.name}</span>
        </div>
        <span className="dish-details-constructor-price">
          +{item.price} грн
        </span>
      </div>
    ),
  );
  return (
    <div className="dish-details-constructor">
      <p className="dish-details-constructor-title">
        Додайте або видаліть інгредієнт
      </p>
      <div className="dish-details-constructor-list">
        {constructorItemElements}
      </div>
    </div>
  );
};

export default DishDetailsConstructor;
