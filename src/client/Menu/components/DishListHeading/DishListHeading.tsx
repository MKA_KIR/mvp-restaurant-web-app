import React from 'react';
import './DishListHeading.scss';

// @ts-ignore
// eslint-disable-next-line react/prop-types
const DishListHeading = ({ icon, name }) => {
  return (
    <div className="dish-list-heading">
      <span className="dish-list-icon-container">
        <img src={icon} alt={name} className="dish-list-icon" />
      </span>
      <h2 className="dish-list-title">{name}</h2>
    </div>
  );
};

export default DishListHeading;
