import React from 'react';

import { ReactComponent as FireAlt } from '../../../../../../mvp-arena-backend/public/images/categories/fireAlt.svg';
import { ReactComponent as Leaf } from '../../../../../../mvp-arena-backend/public/images/categories/leaf.svg';
import { ReactComponent as Drumstick } from '../../../../../../mvp-arena-backend/public/images/categories/drumstick.svg';
import popularFoodImg from '../../../../assets/images/category-list-page/popular-food.png';
import saladImg from '../../../../assets/images/category-list-page/salad.png';
import meatImg from '../../../../assets/images/category-list-page/meat.png';

export const categories = [
  {
    name: 'Популярні страви',
    icon: <FireAlt />,
    img: popularFoodImg,
  },
  {
    name: 'Салати',
    icon: <Leaf />,
    img: saladImg,
  },
  {
    name: "М'ясо",
    icon: <Drumstick />,
    img: meatImg,
  },
];
