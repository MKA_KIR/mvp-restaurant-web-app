import React, { useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';

import { getCategories } from '../../action';

import CategoryListItem from '../CategoryListItem';

const CategoryList = () => {
  // @ts-ignore
  const categories = useSelector(({ menu }) => menu.categories, shallowEqual);
  // @ts-ignore
  const loading = useSelector(({ menu }) => menu.loading, shallowEqual);

  const dispatch = useDispatch();
  // @ts-ignore
  useEffect(() => dispatch(getCategories()), []);

  // @ts-ignore
  const categoryElements = categories.map(({ id, ...categoryProps }) => (
    <CategoryListItem key={id} {...categoryProps} />
  ));
  return (
    <div className="container">
      {loading && <p>loading...</p>}
      <div className="category-list">{categoryElements}</div>
    </div>
  );
};

export default CategoryList;
