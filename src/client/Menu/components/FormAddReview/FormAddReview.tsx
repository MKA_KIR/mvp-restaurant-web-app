import React from 'react';
import { Form, Formik, Field } from 'formik';
import './FormAddReview.scss';

const FormAddReview = () => {
  return (
    <Formik
      initialValues={{ reviewText: '' }}
      onSubmit={(values) => console.log(values)}
    >
      <Form>
        <Field className="send-review-textarea send-review-field" as="textarea" row="5" name="reviewText" placeholder="Ваш Відгук" />
      </Form>
    </Formik>
  );
};

export default FormAddReview;
