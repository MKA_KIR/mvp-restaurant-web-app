import React, { FC, PropsWithChildren } from 'react';
import './DishDetailsSlider.scss';

export interface IDishDetailsSlider {
  imgList: string[];
}

const DishDetailsSlider: FC<PropsWithChildren<IDishDetailsSlider>> = ({
  imgList,
}: Partial<IDishDetailsSlider>) => {
  // @ts-ignore
  const sliderItemElements = imgList.map((item) => {
    const dishBgImg = {
      backgroundImage: `url(${item})`,
    };
    return <div className="dish-details-slider-item" style={dishBgImg} />;
  });
  return <div className="dish-details-slider">{sliderItemElements}</div>;
};
export default DishDetailsSlider;
