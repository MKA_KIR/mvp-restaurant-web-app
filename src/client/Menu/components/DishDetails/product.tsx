import saladeGoose
  from '../../../../../../mvp-arena-backend/public/images/product/salad-goose.png';
import saladImg from '../../../../../../mvp-arena-backend/public/images/product/salad.png';
import ellipse from '../../../../../../mvp-arena-backend/public/images/product/reviews/author-avatars/avatar1.png';

export interface IRelatedProducts {
  _id: string;
  name: string;
  price: number;
}

export interface IProductIngredients {
  name: string;
  price: number;
}

export interface IProductDetails {
  time: number;
  calories: number;
  description: string;
}

export interface IProduct {
  imgList: string[];
  name: string;
  price: number;
  details: IProductDetails;
  relatedProducts: IRelatedProducts[];
  galleryProducts: IGaleryProducts[];
  reviewList: IReviewList[];
}

export interface IGaleryProducts {
  _id: string;
  img: string;
  name: string;
  price: number;
}

export interface IReviewList {
  _id: string;
  authorAvatar: string;
  authorName: string;
  rating: number;
  date: string;
  text: string;
}

export const product: IProduct = {
  imgList: [saladeGoose],
  name: 'Зелений салат з яблуком та яловичиною',
  price: 190,
  details: {
    time: 50,
    calories: 343,
    description:
      'хлеб бриошь,ветчина, сыр чеддер, жареное яйцо, голандский соус, горчица Дижон) подается с маринованными огурцами',
  },
  relatedProducts: [
    {
      _id: '2323',
      name: 'Соус Чеддер',
      price: 30,
    },
    {
      _id: '2424',
      name: 'Сыр Пармезан',
      price: 20,
    },
  ],
  galleryProducts: [
    {
      _id: '1111',
      img: saladImg,
      name: 'Салат із печеним перцем та авокадо',
      price: 190,
    },
    {
      _id: '2222',
      img: saladImg,
      name: 'Домашня ковбаса',
      price: 190,
    },
    {
      _id: '3333',
      img: saladImg,
      name: 'Таріль сала',
      price: 190,
    },
  ],
  reviewList: [
    {
      _id: '1212',
      authorAvatar: ellipse,
      authorName: 'Anastasiya',
      rating: 5,
      date: '22.11.2020',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis',
    },
    {
      _id: '1313',
      authorAvatar: ellipse,
      authorName: 'Anastasiya',
      rating: 5,
      date: '22.11.2020',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis',
    },
    {
      _id: '1414',
      authorAvatar: ellipse,
      authorName: 'Anastasiya',
      rating: 5,
      date: '22.11.2020',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis',
    },
  ]
};
