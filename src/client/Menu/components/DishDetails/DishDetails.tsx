import React, { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useParams, useLocation } from 'react-router-dom';
import { v4 } from 'uuid';

import './DishDetails.scss';

import DishDetailsSlider from '../DishDetailsSlider';
import Tabs from '../../../../shared/components/Tabs';
import DishActions from '../DishActions';
import Gallery from '../Gallery';
import Reviews from '../Reviews';

import { getSingleProduct } from '../../action';
import DishDetailsInfo from '../DishDetailsInfo';
import DishDetailsConstructor from '../DishDetailsConstructor';

const DishDetails = () => {
  const { id } = useParams();
  const { state } = useLocation();

  const [productCount, setProductCount] = useState(state ? state.count : 1);

  const increment = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    setProductCount((prevState: number) => prevState + 1);
  };
  const decrement = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    setProductCount((prevState: number) =>
      prevState === 1 ? 1 : prevState - 1,
    );
  };
  // @ts-ignore
  const product = useSelector(({ menu }) => menu.product, shallowEqual);

  // @ts-ignore
  const loading = useSelector(({ menu }) => menu.loading, shallowEqual);
  const dispatch = useDispatch();
  // @ts-ignore
  useEffect(() => dispatch(getSingleProduct(id)), [id, dispatch]);

  if (!product || loading) {
    // FullProduct ставим продукт и все нормально перезагружается
    return <p>loading...</p>;
  } else {
    const {
      active,
      imgList,
      name,
      price,
      details,
      additionalIngridients,
      relatedProducts,
      reviewList,
    } = product; // FullProduct перезаписывает из одного состояния в другое и 2 стостояние нал

    const relatedProductsGallery = relatedProducts ? (
      <Gallery
        name="З цією стравою також замовляють:"
        galleryProducts={relatedProducts}
      />
    ) : (
      ''
    );

    // @ts-ignore
    const tabsContent = [
      {
        id: v4(),
        title: 'Деталі',
        component: <DishDetailsInfo {...details} />,
      },
      {
        id: v4(),
        title: 'Корекція',
        component: (
          <DishDetailsConstructor
            additionalIngridients={additionalIngridients}
          />
        ),
      },
    ];
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return (
      <main className="dish-details">
        <DishDetailsSlider imgList={imgList} />
        <div className="container">
          <p className="dish-details-name">{name}</p>
          <p className="dish-details-price">{price} грн</p>
          <Tabs content={tabsContent} />
          <DishActions
            active={active}
            count={productCount}
            increment={increment}
            decrement={decrement}
            type="full"
          />
          {relatedProductsGallery}
          <Reviews reviewList={reviewList} />
        </div>
      </main>
    );
  }
};

export default DishDetails;
