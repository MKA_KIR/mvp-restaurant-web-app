export { ReactComponent as StopWatch } from '../../../../assets/images/dish-page-info/stopwatch.svg';
export { ReactComponent as BalanceScale } from '../../../../assets/images/dish-page-info/balance-scale.svg';
export { ReactComponent as ListAlt } from '../../../../assets/images/dish-page-info/list-alt.svg';
