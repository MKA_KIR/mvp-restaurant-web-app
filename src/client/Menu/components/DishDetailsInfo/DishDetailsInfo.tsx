import React, { FC, PropsWithChildren } from 'react';

import { StopWatch, BalanceScale, ListAlt } from './infoIcons';

import './DishDetailsInfo.scss';

export interface IDishDetailsInfoProps {
  time: any;
  calories: any;
  description: any;
}

const DishDetailsInfo: FC<PropsWithChildren<IDishDetailsInfoProps>> = ({
  time,
  calories,
  description,
}: Partial<IDishDetailsInfoProps>) => {
  const hours = Math.floor(time / 60);
  const minutes = time - hours * 60;
  const fullTime =
    hours > 0 ? `${hours} год. ${minutes} хв.` : `${minutes} хв.`;
  return (
    <div>
      <div className="dish-details-info-item">
        <div className="dish-details-info-icon-container">
          <StopWatch />
        </div>
        <p className="dish-details-info-text">{fullTime}</p>
      </div>
      <div className="dish-details-info-item">
        <div className="dish-details-info-icon-container">
          <BalanceScale />
        </div>
        <p className="dish-details-info-text">{calories} калорії</p>
      </div>
      <div className="dish-details-info-item">
        <div className="dish-details-info-icon-container">
          <ListAlt />
        </div>
        <p className="dish-details-info-text">{description}</p>
      </div>
    </div>
  );
};

export default DishDetailsInfo;
