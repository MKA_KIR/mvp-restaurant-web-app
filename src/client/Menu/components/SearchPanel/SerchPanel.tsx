import React, { useState, useEffect } from 'react';
import { Field, Form, Formik } from 'formik';
import axios from 'axios';

import './SerchPanel.scss';

import { ReactComponent as SearchIcon } from '../../../../assets/images/icons/search.svg';

const SearchPanel = () => {
  const [searchText, setSearchText] = useState('');
  const [searchResult, setSearchResult] = useState([]);
  const handleChange = ({ target }: any) => {
    setSearchText(target.value);
  };
  // @ts-ignore
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(async () => {
    if (searchText.length > 1) {
      const { data } = await axios.get(
        `http://localhost:3000/products?q=${searchText}`,
      );
      setSearchResult(data);
    }
  }, [searchText]);
  const searchResultElements = searchResult.map(({ id, name }) => (
    <li className="search-results-item" key={id}>
      {name}
    </li>
  ));
  return (
    //@ts-ignore
    <div className="container">
      <div className="search-panel">
        <Formik initialValues={{ search: '' }} onSubmit={(values) => {}}>
          <Form>
            <div className="input-group">
              <Field>
                {/* eslint-disable-next-line @typescript-eslint/no-unused-vars */}
                {(values: any) => (
                  <input
                    onChange={handleChange}
                    className="input-group-field"
                    placeholder="Пошук"
                    name="search"
                  />
                )}
              </Field>
              <div className="input-group-icon-append">
                <SearchIcon className="input-group-icon" />
              </div>
            </div>
          </Form>
        </Formik>
        <ul className="search-results">{searchResultElements}</ul>
      </div>
    </div>
  );
};
export default SearchPanel;
