import React, { FC, PropsWithChildren } from 'react';
import './ReviewsItem.scss';
import { ReactComponent as Star } from '../../../../assets/images/dish-page-info/star.svg';

interface IReviewsItem {
  authorAvatar: any;
  authorName: any;
  rating: any;
  date: any;
  text: any;
}

const ReviewsItem: FC<PropsWithChildren<IReviewsItem>> = ({
  authorAvatar,
  authorName,
  rating,
  date,
  text,
}: Partial<IReviewsItem>) => {
  const ratingElements = Array(Math.floor(rating))
    .fill(<Star />)
    .map((item) => <li className="review-rating-star">{item}</li>);
  return (
    <div className="reviews-item">
      <div className="review-author">
        <div
          className="review-author-avatar"
          style={{ backgroundImage: `url(${authorAvatar})` }}
        ></div>
        <p className="review-author-name">{authorName}</p>
      </div>
      <div className="review-details">
        <ul className="review-rating">{ratingElements}</ul>
        <div className="review-date">{date}</div>
      </div>
      <div className="review-text">{text}</div>
      <div className="review-actions">
        <p className="review-actions-name">Чи був цей відгук корисним? </p>
        <div className="review-actions-buttons">
          <a href="#" className="review-btn review-btn-primary">
            Так
          </a>
          <a href="#" className="review-btn review-btn-outline">
            Ні
          </a>
        </div>
      </div>
    </div>
  );
};

export default ReviewsItem;
