import {
  FETCH_REQUEST,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_FAILURE,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_SINGLE_PRODUCT_SUCCESS,
  FETCH_SINGLE_CATEGORY_SUCCESS,
  CHANGE_PRODUCT_PRICE,
} from '../constants';

const initialState = {
  products: [],
  product: null,
  categories: [],
  category: null,
  loading: false,
  error: null,
  message: null,
};

// eslint-disable-next-line import/prefer-default-export
export const menuReducer = (
  state = initialState,
  action: {
    payload: null;
    type: any;
  },
) => {
  switch (action.type) {
    case FETCH_REQUEST:
      return { ...state, loading: true };
    case FETCH_CATEGORIES_SUCCESS:
      // @ts-ignore
      return { ...state, categories: action.payload, loading: false };
    case FETCH_FAILURE:
      // @ts-ignore
      return {
        ...state,
        loading: false,
        error: action.payload,
        message: 'error',
      };
    case FETCH_PRODUCTS_SUCCESS:
      // @ts-ignore
      return { ...state, products: action.payload, loading: false };
    case FETCH_SINGLE_PRODUCT_SUCCESS:
      // @ts-ignore
      return { ...state, product: action.payload, loading: false };
    case CHANGE_PRODUCT_PRICE:
      // @ts-ignore
      const newProduct = { ...state.product };
      newProduct.price += action.payload;
      return { ...state, product: newProduct, loading: false };
    case FETCH_SINGLE_CATEGORY_SUCCESS:
      // @ts-ignore
      return { ...state, category: action.payload, loading: false };

    default:
      return state;
  }
};
