import React, { useState } from 'react';
import './Tabs.scss';

// @ts-ignore
const Tabs = ({ content }) => {
  const [activeTab, setActiveTab] = useState(0);

  const handleToggle = (idx: any) => setActiveTab(idx);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const tabsElements = content.map(({ id, title }: any, index: number) => {
    const aditionalClassName = index === activeTab ? 'active' : '';
    const fullClassName = `tabs-menu-item ${aditionalClassName}`;
    return (
      <li className={fullClassName} onClick={() => handleToggle(index)}>
        {/* eslint-disable-next-line no-restricted-globals */}
        {title}
      </li>
    );
  });
  return (
    <div className="tabs">
      <ul className="tabs-menu">{tabsElements}</ul>
      <div className="tabs-item">{content[activeTab].component}</div>
    </div>
  );
};

export default Tabs;
