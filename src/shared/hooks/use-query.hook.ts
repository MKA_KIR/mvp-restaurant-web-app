import { useMemo } from 'react';

import useRouterHook from './use-router.hook';

const useQuery = () => {
  const { query } = useRouterHook();

  return useMemo(() => {
    return {
      page: 1,
      limits: 20,
      ...query,
    };
  }, [query]);
};

export default useQuery;
