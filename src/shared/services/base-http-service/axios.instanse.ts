import axios from 'axios';
// import interceptors from './axios.interceptors';

const instance = axios.create();

// interceptors(instance);

export default instance;
