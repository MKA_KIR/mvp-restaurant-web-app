import { AxiosInstance, AxiosResponse } from 'axios';

import instance from './axios.instanse';

export const updateToken = (refreshToken: string): Promise<any> => {
  return new Promise((resolve) => {
    resolve(
      instance.put('/token/update', {
        refreshToken,
      }),
    );
  });
};

const checkTokenAndUpdate = (response: AxiosResponse): AxiosResponse => {
  return response;
};

let isAlreadyFetchingAccessToken = false;
let subscribers: any[] = [];

function onAccessTokenFetched(accessToken: string) {
  subscribers = subscribers.filter((callback) => callback(accessToken));
}

function addSubscriber(callback: any) {
  subscribers.push(callback);
}

export default (axios: AxiosInstance) => {
  axios.interceptors.response.use(checkTokenAndUpdate, (error: any) => {
    const {
      config,
      response: { status },
    } = error;
    const loginUrl = config.url.includes('signin');
    const adminUrl = config.url.includes('admin');
    const isLogoutUrl = config.url.includes('logout');

    if (adminUrl && !loginUrl && !isLogoutUrl) {
      const originalRequest = config;
      const expiresIn = Number(localStorage.getItem('expiresIn'));
      if (status === 401) {
        if (Date.now() / 1000 > expiresIn) {
          localStorage.clear();
          window.location.reload();
        } else {
          if (!isAlreadyFetchingAccessToken) {
            isAlreadyFetchingAccessToken = true;
            updateToken(String(localStorage.getItem('refreshToken'))).then(
              (accessToken: any) => {
                isAlreadyFetchingAccessToken = false;
                onAccessTokenFetched(accessToken.data.accessToken);
              },
            );
          }

          return new Promise((resolve) => {
            addSubscriber((accessToken: any) => {
              localStorage.setItem('accessToken', accessToken);
              originalRequest.headers.Authorization = accessToken;
              resolve(axios(originalRequest));
            });
          });
        }
      }
    }
    return Promise.reject(error);
  });
};
