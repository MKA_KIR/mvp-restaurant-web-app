import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

// import axios from './axios.instanse';

export interface TokenInterface {
  accessToken: string;
  refreshToken: string;
  expiresIn: string;
}

export default class BaseHttpService {
  BASE_URL = 'https://cmusy.space/api/v1';

  _accessToken: string | null = null;

  _refreshToken: string | null = null;

  _expiresIn: string | null = null;

  async get(endpoint: string, options: AxiosRequestConfig = {}): Promise<any> {
    Object.assign(options, this._getCommonOptions());
    return axios
      .get(`${this.BASE_URL}/${endpoint}`, options)
      .then((res) => res.data)
      .catch((error) => this._handleHttpError(error));
  }

  async post(
    endpoint: string,
    data = {},
    options: AxiosRequestConfig = {},
  ): Promise<AxiosResponse | void> {
    Object.assign(options, this._getCommonOptions());
    return axios
      .post(`${this.BASE_URL}/${endpoint}`, data, options)
      .catch((error) => this._handleHttpError(error));
  }

  async put(
    endpoint: string,
    data = {},
    options: AxiosRequestConfig = {},
  ): Promise<AxiosResponse | any> {
    Object.assign(options, this._getCommonOptions());
    return axios
      .put(`${this.BASE_URL}/${endpoint}`, data, options)
      .catch((error) => this._handleHttpError(error));
  }

  async delete(
    endpoint: string,
    options: AxiosRequestConfig = {},
  ): Promise<AxiosResponse | void> {
    Object.assign(options, this._getCommonOptions());
    return axios
      .delete(`${this.BASE_URL}/${endpoint}`, options)
      .catch((error) => this._handleHttpError(error));
  }

  async patch(
    endpoint: string,
    data = {},
    options: AxiosRequestConfig = {},
  ): Promise<AxiosResponse | any> {
    Object.assign(options, this._getCommonOptions());
    return axios
      .patch(`${this.BASE_URL}/${endpoint}`, data, options)
      .catch((error) => this._handleHttpError(error));
  }

  _handleHttpError(error: any) {
    const { statusCode } = error.response.data;
    if (statusCode !== 401) {
      throw error;
    } else {
      return this._handle401();
    }
  }

  _handle401 = () => {
    this._accessToken = null;
    this._refreshToken = null;
    this._expiresIn = null;
    this.removeToken();
    window.location.replace('/');
  };

  _getCommonOptions = (): AxiosRequestConfig => {
    const { accessToken } = this.loadToken();

    return {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    } as AxiosRequestConfig;
  };

  get accessToken() {
    return this._accessToken ? this._accessToken : this.loadToken();
  }

  saveToken = (data: TokenInterface): TokenInterface => {
    const { accessToken, expiresIn, refreshToken } = data;
    this._accessToken = accessToken;
    this._refreshToken = refreshToken;
    this._expiresIn = expiresIn;

    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('refreshToken', refreshToken);
    localStorage.setItem('expiresIn', expiresIn);

    return {
      accessToken,
      refreshToken,
      expiresIn,
    };
  };

  loadToken(): TokenInterface {
    const accessToken = localStorage.getItem('accessToken') as string;
    const refreshToken = localStorage.getItem('refreshToken') as string;
    const expiresIn = localStorage.getItem('expiresIn') as string;

    this._accessToken = accessToken;
    this._refreshToken = refreshToken;
    this._expiresIn = expiresIn;

    return {
      accessToken,
      refreshToken,
      expiresIn,
    };
  }

  removeToken = (): void => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('expiresIn');
  };
}
