import React, { FC, memo, PropsWithChildren } from 'react';
import { FormikValues } from 'formik';
import { Typography } from 'antd';

const FormDataPreview: FC<PropsWithChildren<FormikValues>> = ({
  values,
}: Partial<FormikValues>) => {
  return (
    <Typography.Text code>
      <pre>{JSON.stringify(values, null, 2)}</pre>
    </Typography.Text>
  );
};

export default memo(FormDataPreview);
