import React, { FC, memo, PropsWithChildren } from 'react';
import { Input, Form } from 'antd';
import { FormikErrors, FormikTouched, FormikValues } from 'formik';

export interface IFormControlProps {
  type: 'text' | 'email' | 'password';
  name: string;
  label: string;
  errors: FormikErrors<any>;
  touched: FormikTouched<any>;
  handleChange: any;
  handleBlur: any;
  values: FormikValues;
}

const FormControlCreator: FC<PropsWithChildren<IFormControlProps>> = (
  props: IFormControlProps,
) => {
  const {
    type,
    name,
    label,
    handleBlur,
    handleChange,
    errors,
    touched,
    values,
  } = props;
  switch (type) {
    case 'password':
      return (
        <Form.Item
          label={label}
          name={name}
          help={errors[name] && touched[name] && errors[name]}
          hasFeedback={Boolean(errors[name] && touched[name])}
          validateStatus={errors[name] && touched[name] ? 'error' : 'success'}
        >
          <Input.Password
            type={type}
            name={name}
            onBlur={handleBlur}
            onChange={handleChange}
            value={values[name]}
          />
        </Form.Item>
      );
    case 'email':
      return (
        <Form.Item
          name={name}
          label={label}
          help={errors[name] && touched[name] && errors[name]}
          hasFeedback={Boolean(errors[name] && touched[name])}
          validateStatus={errors[name] && touched[name] ? 'error' : 'success'}
        >
          <Input
            type={type}
            name={name}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Form.Item>
      );
    default:
      return (
        <Form.Item
          name={name}
          label={label}
          help={errors[name] && touched[name] && errors[name]}
          hasFeedback={Boolean(errors[name] && touched[name])}
          validateStatus={errors[name] && touched[name] ? 'error' : 'success'}
        >
          <Input
            type={type}
            name={name}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Form.Item>
      );
  }
};

export default memo(FormControlCreator);
