export interface IFieldsProps {
  type: 'text' | 'email' | 'password';
  name: string;
  label: string;
}

const fields: IFieldsProps[] = [
  {
    type: 'email',
    name: 'email',
    label: 'Email',
  },
  {
    type: 'password',
    name: 'password',
    label: 'Password',
  },
];

export default fields;
