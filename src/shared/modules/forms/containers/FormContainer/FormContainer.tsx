import React, { FC, memo, PropsWithChildren } from 'react';
import { FormikValues, useFormik } from 'formik';
import { Button, Form } from 'antd';

import FormControlCreator from '../../components/FormControlCreator';
import FormDataPreview from '../../components/FormDataPreview';

export interface IDataProps {
  type: 'text' | 'email' | 'password';
  name: string;
  label: string;
}

export interface IFormContainerProps {
  onSubmit: any;
  data: IDataProps[];
  validationSchema: Record<string, any>;
  initialValues: FormikValues;
  debug?: boolean;
}

const FormContainer: FC<PropsWithChildren<IFormContainerProps>> = (
  props: IFormContainerProps,
) => {
  const { onSubmit, initialValues, validationSchema, data, debug } = props;
  const {
    handleChange,
    handleBlur,
    errors,
    touched,
    values,
    handleSubmit,
  } = useFormik({
    onSubmit: (values) => onSubmit(values),
    validationSchema,
    initialValues,
  });

  return (
    <>
      {debug && <FormDataPreview values={values} />}
      <Form autoComplete="off" noValidate onFinish={handleSubmit}>
        {data.map((field) => (
          <FormControlCreator
            key={field.name}
            errors={errors}
            touched={touched}
            handleChange={handleChange}
            handleBlur={handleBlur}
            values={values}
            {...field}
          />
        ))}
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form>
    </>
  );
};

export default memo(FormContainer);
