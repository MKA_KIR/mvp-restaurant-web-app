import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Button, Result } from 'antd';

import { fetchProducts } from '../../reducer/products.reducer';
import useProductsSelector from '../../selectors/products.selector';
import ProductsList from '../../components/ProductsList';

const ProductsTableContainer = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  const { products, productsError, productsRequested } = useProductsSelector();

  if (productsError) {
    return (
      <Result
        status="500"
        title="500"
        subTitle="Sorry, something went wrong."
        extra={<Button type="primary">Back Home</Button>}
      />
    );
  }

  return (
    <div>
      <h1>Products Table Container</h1>
      <ProductsList products={products} loading={productsRequested} />
    </div>
  );
};

export default ProductsTableContainer;
