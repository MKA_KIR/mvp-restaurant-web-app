import React from 'react';

import ProductsTableContainer from '../../containers/ProductsTableContainer';

const ProductsPage = () => {
  return (
    <div>
      <ProductsTableContainer />
    </div>
  );
};

export default ProductsPage;
