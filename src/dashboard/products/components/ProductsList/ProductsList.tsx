import React, { FC, PropsWithChildren } from 'react';
import { Avatar, List } from 'antd';
import { ProductInterface } from '../../interfaces/products.interface';

interface IProductsList {
  products: ProductInterface[];
  loading: boolean;
}

const ProductsList: FC<PropsWithChildren<IProductsList>> = ({
  products,
  loading,
}: Partial<IProductsList>) => {
  return (
    <List
      itemLayout="horizontal"
      dataSource={products}
      loading={loading}
      renderItem={(item) => (
        <List.Item>
          <List.Item.Meta
            avatar={<Avatar src={item.thumbnail} />}
            title={<a href="https://ant.design">{item.title}</a>}
            description={item.title}
          />
        </List.Item>
      )}
    />
  );
};

export default ProductsList;
