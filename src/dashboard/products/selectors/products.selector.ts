import { shallowEqual, useSelector } from 'react-redux';
import { RootState } from '../../../app/store/root-reducer';

const useProductsSelector = () => {
  const { products, productsError, productsRequested } = useSelector(
    (state: RootState) => {
      return {
        productsRequested: state.products.loading,
        productsError: state.products.error,
        products: state.products.products,
      };
    },
    shallowEqual,
  );
  return {
    products,
    productsError,
    productsRequested,
  };
};

export default useProductsSelector;
