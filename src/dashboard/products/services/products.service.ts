import { AxiosResponse } from 'axios';

import BaseHttpService from '../../../shared/services/base-http-service/base-http.service';

export default class ProductsService extends BaseHttpService {
  fetchProducts(): Promise<AxiosResponse> {
    return this.get(`products`);
  }
}
