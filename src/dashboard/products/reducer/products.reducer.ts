import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ProductInterface } from '../interfaces/products.interface';
import { AppDispatch } from '../../../app/store/configure-store';
import ProductsService from '../services/products.service';

interface ProductsReducerInterface {
  products: [] | ProductInterface[];
  product: ProductInterface;
  loading: boolean;
  error: null | Record<string, any>;
  message: null | string;
}

const products = createSlice({
  name: 'products',
  initialState: {
    products: [],
    product: {},
    loading: true,
    error: null,
    message: null,
  } as ProductsReducerInterface,
  reducers: {
    fetchProductsRequested(state) {
      state.loading = true;
      state.error = null;
    },
    fetchProductsSuccess(state, action: PayloadAction<any>) {
      const { products } = action.payload;
      state.loading = false;
      state.error = null;
      state.products = products;
    },
    fetchProductsFailure(state, action: PayloadAction<any>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  fetchProductsRequested,
  fetchProductsSuccess,
  fetchProductsFailure,
} = products.actions;
export default products.reducer;

const productsService = new ProductsService();

const fetchProducts = () => async (dispatch: AppDispatch) => {
  try {
    dispatch(fetchProductsRequested());
    const products = await productsService.fetchProducts();
    dispatch(fetchProductsSuccess({ products }));
  } catch (errors) {
    dispatch(fetchProductsFailure({ errors }));
  }
};

export { fetchProducts };
