export interface CreateProductInterface {
  name: string;
  sku: string;
  content: string;
  enabled: boolean;
  imageURL: string;
  price: string;
  prop: string;
}

export interface ProductInterface {
  _id: string;
  name: string;
  title: string;
  enabled: boolean;
  sku: string;
  order: string;
  content: string;
  thumbnail: string;
  imageURL: string;
  price: string;
  prop: string;
}
